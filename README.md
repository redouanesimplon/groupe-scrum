## DEV TEAM





![dev team](https://www.devteam.space/wp-content/uploads/2018/09/how-to-build-a-scrum-software-development-team-730x410.png)

L' équipe de développement est un sous-ensemble de l' [équipe agile](https://www.scaledagileframework.com/dev-team/ "équipe agile") . Il est composé de professionnels dédiés qui peuvent développer, tester et déployer une histoire , une fonctionnalité ou un composant. L'équipe de développement comprend généralement des développeurs et des testeurs de logiciels, des ingénieurs et d'autres spécialistes dédiés nécessaires pour compléter une tranche verticale de fonctionnalités. Par souci de cohérence avec la définition de Scrum, l’équipe de développement exclut le propriétaire du produit et le Scrum Master; ils font partie de la plus grande équipe agile.



![dev team](https://camo.githubusercontent.com/62a6b34676c91227c8dd7cfd3ccdbc5be585cdbe/68747470733a2f2f7777772e736166617269626f6f6b736f6e6c696e652e636f6d2f6c6962726172792f766965772f7468652d70726f66657373696f6e616c2d736372756d6d6173746572732f393738313834393638383032342f67726170686963732f383032345f30315f30342e6a7067)

## Détails

Les développeurs et les testeurs sont au cœur du développement Agile. Ils travaillent en petites équipes interfonctionnelles et peuvent créer rapidement un code fonctionnel, testé et qui génère de la valeur. Ils construisent les systèmes dont nous dépendons tous. Dans le développement traditionnel, les rôles de développeur et de testeur sont généralement différenciés, chaque rapport se trouvant dans une structure de gestion différente. Dans Agile, les deux rôles se mélangent. Par exemple, Mike Cohn ne distingue pas les rôles et appelle plutôt tout le monde «développeurs» [1]. Les ingénieurs qui développent du matériel, des microprogrammes et d'autres composants sont également considérés comme des «développeurs» dans le contexte de SAFe. La ligne entre le développement et le test est délibérément floue. Les développeurs testent et testent le code. Les équipes de développement sont habilitées par l'entreprise à gérer et à s'auto-organiser pour accomplir leur propre travail. Ils incluent toutes les compétences dont l’équipe a besoin pour créer une solution efficace et testée.

## Responsabilité



![dev team](http://www.nomadeis.com/dl/2016/03/Fotolia_70497889_XS.jpg)

Les responsabilités de l'équipe de développement incluent: 

* Collaborer avec le responsable de produit  pour créer et affiner les user stories et les critères d'acceptation 

* Participer à PI Planification et création de plans de itération et  d'objectifs d'équipe PI 

* Développer et s’engager aux objectifs de l’ équipe PI et aux objectifs de l’ itération 

* Travailler avec le responsable de produit  pour confirmer que le code et les tests d'acceptation reflètent la fonctionnalité souhaitée; écrire le code

* Effectuer des activités de recherche, de conception, de prototypage et autres activités d'exploration 

* Création de tests unitaires et de tests d'acceptation automatisés 

* Utilisation des meilleures pratiques de conception et de codage pour créer des composants et des solutions de haute qualité .

* Vérification du nouveau code dans le référentiel de code source partagé 

* Couplage pour écrire du code et des cas de tests d'acceptation automatisés
* Exécution des tests d'acceptation et maintenance des scénarios de test dans un référentiel partagé 

* Amélioration continue du processus de l'équipe 

## Collocation, collaboration et partage de connaissances

![alt text](https://devsquad.com/images/main_expand_your_team.png "Logo Title Text 1")



La co-localisation des membres de l'équipe Agile et le flou des rôles traditionnels optimisent la vitesse et la qualité. Cela aide également à créer des équipes agiles habilitées. Cependant, cela signifie que les développeurs ne travaillent plus collectivement à partir d'un pool de ressources partagées, ce qui permet de dire qu'il était plus facile d'apprendre, de partager et de faire progresser les compétences collectives. Pour résoudre ce problème, l’entreprise Agile doit créer consciemment une culture et un environnement où les meilleures pratiques et les connaissances sont partagées. Cela inclut de nouvelles compétences Agile retrouvée telles que l' écriture de l' histoire,  l' exploration continue , l' intégration continue , le déploiement continu , la propriété collective du code, et l' unité automatique et les tests d'acceptation, qui sont facilement partagées entre les équipes. Ceci est souvent facilité parCommunautés de pratique . 

## Livraison continue

Comme indiqué ci-dessus, l'équipe de développement est directement responsable d'une grande partie de la culture et de nombreuses pratiques nécessaires à la création du  pipeline de distribution continue et à la mise en œuvre de DevOps . À cette fin, leur formation «en forme de T» comprend également le développement d’une expertise en matière de développement et de gestion d’environnements de déploiement et de déploiement et de techniques de maîtrise pour la libération indépendante d’éléments de la solution plus vaste . Ils assument des responsabilités supplémentaires pour suivre le code en aval et en production. Cela allie davantage les responsabilités traditionnelles cloisonnées de sorte que les équipes Agile, et même les développeurs individuels, puissent maîtriser la capacité de libération sur demande .

## Construire des composants et des solutions de haute qualité



![alt text](https://pronto-core-cdn.prontomarketing.com/2/wp-content/uploads/sites/1824/2017/09/blog-img-ensuring-your-app-dev-team.jpg "Logo Title Text 1")

Les équipes de développement sont implacables sur la qualité et  intègrent la qualité dans leurs artefacts et pratiques de développement. Ils suivent les pratiques du test d’abord lorsqu’ils écrivent des tests de code ( Test-Driven-Development ) et des tests de réception de leurs histoires ( Behavior-Driven-Development ). Les équipes de développement agiles appliquent des pratiques de conception de qualité (abstraction, encapsulation, SOLID, modèles de conception) et de mise en œuvre (programmation extrême) afin de pouvoir répondre rapidement et de manière fiable aux nouveaux objectifs de l'entreprise. Et ils repensent sans relâche leurs conceptions pour pouvoir répondre en permanence aux nouvelles exigences. Reportez-vous à la section relative à l'agilité technique de l'article sur les compétences d' équipe et d'agilité technique de SAFe pour plus d'informations. 





![twitter](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS_Gvuu7S-J6p1mmQQ1DlIj4p0a-AH3UjiLI8x7J_l9J7RFY3zWrA)[Twitter](https://twitter.com/scrumdotorg)

 ![youtube](https://articles-images.sftcdn.net/wp-content/uploads/sites/9/2012/02/youtube-logo.png)  [Youtube](https://www.youtube.com/watch?v=vLqCkj0PvtE)

